<?php $bavotasan_theme_options = bavotasan_theme_options(); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div id="page" class="grid <?php echo esc_attr( $bavotasan_theme_options['width'] ); ?>">

		<header id="header" class="row" role="banner">
			<div id="mobile-menu">
				<a href="#" class="left-menu"><i class="icon-reorder"></i></a>
				<a href="#"><i class="icon-search"></i></a>
			</div>
			<div id="drop-down-search"><?php get_search_form(); ?></div>

			<div class="c12">

				<div class="header-wrap">
					<?php $tag = ( is_front_page() && is_home() ) ? 'h1' : 'div'; ?>
					<<?php echo $tag; ?> id="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></<?php echo $tag; ?>>
					<?php if ( $bavotasan_theme_options['tagline'] ) { ?><div id="site-description"><?php bloginfo( 'description' ); ?></div><?php } ?>
				</div>

				<?php
				// Header Widgetized Area
				dynamic_sidebar( 'header-area' );

				$header_image = get_header_image();
				if ( ! empty( $header_image ) ) :
					?>
					<a href="<?php echo esc_url( home_url() ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img id="header-img" src="<?php echo esc_url( $header_image ); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="" /></a>
					<?php
				endif;
				?>

				<div id="nav-wrapper">
					<div class="nav-content">
						<nav id="site-navigation" role="navigation">
							<h3 class="screen-reader-text"><?php _e( 'Main menu', 'gridiculous' ); ?></h3>
							<a class="screen-reader-text" href="#primary" title="<?php esc_attr_e( 'Skip to content', 'gridiculous' ); ?>"><?php _e( 'Skip to content', 'gridiculous' ); ?></a>
							<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '' ) ); ?>
						</nav><!-- #site-navigation -->
					</div>
				</div>
			</div><!-- .c12 -->

		</header><!-- #header .row -->

		<main id="main" class="row">
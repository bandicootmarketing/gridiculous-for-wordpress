<?php
/**
 * The template for displaying article footers
 *
 * @since 1.0.8
 */
 if ( is_singular() ) {
	?>
	<footer class="entry">
	    <?php
	    wp_link_pages( array( 'before' => '<p id="pages">' . __( 'Pages:', 'gridiculous' ) ) );
	    edit_post_link( __( '(edit)', 'gridiculous' ), '<p class="edit-link">', '</p>' );
		the_tags( '<p class="tags"><span>' . __( 'Tags:', 'gridiculous' ) . '</span>', ' ', '</p>' );
	    ?>
	</footer><!-- .entry -->
	<?php
}
#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: Gridiculous\n"
"POT-Creation-Date: 2016-02-29 15:06-0500\n"
"PO-Revision-Date: 2016-02-29 15:06-0500\n"
"Last-Translator: c.bavota <cbavota@gmail.com>\n"
"Language-Team: c.bavota <cbavota@gmail.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7\n"
"X-Poedit-Basepath: ../..\n"
"X-Poedit-WPHeader: style.css\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: 404.php:8
msgid "404 Error"
msgstr ""

#: 404.php:11
msgid "Sorry. We can't seem to find the page you are looking for."
msgstr ""

#: comments.php:3
msgid ""
"This post is password protected. Enter the password to view any comments."
msgstr ""

#: comments.php:19
#, php-format
msgid "1 comment for &ldquo;%2$s&rdquo;"
msgid_plural "%1$s comments for &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: comments.php:26 comments.php:38
msgid "Comment navigation"
msgstr ""

#: comments.php:27 comments.php:39
msgid "&larr; Older Comments"
msgstr ""

#: comments.php:28 comments.php:40
msgid "Newer Comments &rarr;"
msgstr ""

#: comments.php:50
msgid "Comments are closed."
msgstr ""

#: content-aside.php:9
msgid "Aside"
msgstr ""

#: content-aside.php:12 content-image.php:16 content-link.php:12
#: content-quote.php:11 content-status.php:20 content.php:17 functions.php:538
#: page.php:29
msgid "Read more &rarr;"
msgstr ""

#: content-footer.php:11
msgid "Pages:"
msgstr ""

#: content-footer.php:12 functions.php:436 functions.php:458
msgid "(edit)"
msgstr ""

#: content-footer.php:13
msgid "Tags:"
msgstr ""

#: content-gallery.php:35
#, php-format
msgid "This gallery contains <a %1$s>%2$s photo &rarr;</a>"
msgid_plural "This gallery contains <a %1$s>%2$s photos &rarr;</a>"
msgstr[0] ""
msgstr[1] ""

#: content-gallery.php:35
#, php-format
msgid "Permalink to %s"
msgstr ""

#: content-header.php:25
#, php-format
msgid "by %s"
msgstr ""

#: content-header.php:26
#, php-format
msgid "Posts by %s"
msgstr ""

#: content-header.php:42
msgid "0 Comments"
msgstr ""

#: content-header.php:42
msgid "1 Comment"
msgstr ""

#: content-header.php:42
msgid "% Comments"
msgstr ""

#: content-link.php:9
msgid "Link"
msgstr ""

#: content-none.php:9
msgid "Nothing found"
msgstr ""

#: content-none.php:12
msgid "No results were found. Please try again."
msgstr ""

#: content-status.php:10
msgid "Status"
msgstr ""

#: content-status.php:17
#, php-format
msgid "Posted on %1$s at %2$s"
msgstr ""

#: footer.php:15
#, php-format
msgid "The %s Theme by %s."
msgstr ""

#: functions.php:59
msgid "Primary Menu"
msgstr ""

#: functions.php:95
msgid "Froggy"
msgstr ""

#: functions.php:335
msgid "First Sidebar"
msgstr ""

#: functions.php:337
msgid ""
"This is the sidebar widgetized area. All defaults widgets work great here."
msgstr ""

#: functions.php:345
msgid "Header Area"
msgstr ""

#: functions.php:347
msgid ""
"Widgetized area in the header to the right of the site name. Great place for "
"a search box or a banner ad."
msgstr ""

#: functions.php:355 library/about.php:148
msgid "Home Page Top Area"
msgstr ""

#: functions.php:357
msgid ""
"Widgetized area on the home page directly below the navigation menu. "
"Specifically designed for 3 text widgets. Must be turned on in the Layout "
"options on the Customize Gridiculous admin page."
msgstr ""

#: functions.php:393
#, php-format
msgid "Page %1$s of %2$s"
msgstr ""

#: functions.php:435
#, php-format
msgid "%1$s at %2$s"
msgstr ""

#: functions.php:440
msgid "Your comment is awaiting moderation."
msgstr ""

#: functions.php:458
msgid "Pingback:"
msgstr ""

#: functions.php:479
msgid "Post Pages menu"
msgstr ""

#: header.php:47
msgid "Main menu"
msgstr ""

#: header.php:48
msgid "Skip to content"
msgstr ""

#: index.php:19
msgid "No posts to display"
msgstr ""

#: index.php:22
#, php-format
msgid "Ready to publish your first post? <a href=\"%s\">Get started here</a>."
msgstr ""

#: library/about.php:26
#, php-format
msgid ""
"Thanks for choosing %s! You can learn how to use the available theme options "
"on the %sabout page%s."
msgstr ""

#: library/about.php:28
#, php-format
msgid "Learn more about %s"
msgstr ""

#: library/about.php:41 library/preview-pro.php:82
#, php-format
msgid "Welcome to %s %s"
msgstr ""

#: library/about.php:41
#, php-format
msgid "About %s"
msgstr ""

#: library/about.php:104
#, php-format
msgid ""
"Read through the following documentation to learn more about <em>%s</em> and "
"how to use the available theme options."
msgstr ""

#: library/about.php:111 library/preview-pro.php:92
msgid "Documentation"
msgstr ""

#: library/about.php:112 library/preview-pro.php:93
msgid "Upgrade"
msgstr ""

#: library/about.php:116
msgid "The Customizer"
msgstr ""

#: library/about.php:122
#, php-format
msgid ""
"All theme options for <em>%s</em> are controlled under %sAppearance &rarr; "
"Customize%s. From there, you can modify layout, custom menus, and more."
msgstr ""

#: library/about.php:124
msgid "Site &amp; Main Content Width"
msgstr ""

#: library/about.php:125
msgid ""
"There are two width options for your site: 1200px &amp; 960px. You can "
"select the width of your main content, based on a 12 column grid. Resizing "
"your main content will also resize your sidebar."
msgstr ""

#: library/about.php:127
msgid "Sidebar Layout"
msgstr ""

#: library/about.php:128
msgid ""
"With the sidebar layout option, you can decide whether to display your "
"sidebar on the left or right of your main content."
msgstr ""

#: library/about.php:130 library/customizer.php:128
msgid "Post Content Display"
msgstr ""

#: library/about.php:131
msgid ""
"You can display your posts on the home/archive pages either with their full "
"content or just a teaser excerpt by selecting one of the two options. If you "
"select the teaser excerpt, then the featured image will be used as a "
"thumbnail next to each post."
msgstr ""

#: library/about.php:136
msgid "Custom Menus"
msgstr ""

#: library/about.php:138
#, php-format
msgid "<em>%s</em> includes one Custom Menu locations: the Primary top menu."
msgstr ""

#: library/about.php:140
#, php-format
msgid ""
"To add a navigation menu to the header, go to %sAppearance &rarr; Menus%s. "
"By default, a list of categories will appear in this location if no custom "
"menu is set."
msgstr ""

#: library/about.php:142
msgid ""
"Hovering over a top-level link in the primary navigation will open up the "
"first dropdown list of sub-menu links."
msgstr ""

#: library/about.php:154
#, php-format
msgid ""
"The section where the three widgets appear on the homepage will only display "
"once a widget is added to the Home Page Top Area in %sAppearance &rarr; "
"Widgets%s. The demo uses the Text widget to display an image accompanied by "
"text. You can even have the image and text link to a page or a post, by "
"adding your own HTML."
msgstr ""

#: library/about.php:159
msgid "Jetpack"
msgstr ""

#: library/about.php:165
msgid "Tiled Galleries"
msgstr ""

#: library/about.php:167
#, php-format
msgid ""
"%sJetpack&rsquo;s Tiled Galleries%s will display your images in a beautiful "
"mosaic layout. Go to %sJetpack &rarr; Settings%s to turn it on. "
msgstr ""

#: library/about.php:169
msgid "Carousel"
msgstr ""

#: library/about.php:171
#, php-format
msgid ""
"With %sJetpack&rsquo;s Carousel%s, clicking on one of your gallery images "
"will open up a featured lightbox slideshow. Turn it on by going to %sJetpack "
"&rarr; Settings%s. "
msgstr ""

#: library/about.php:176
msgid "Pull Quotes"
msgstr ""

#: library/about.php:182
msgid ""
"You can easily include a pull quote within your text by using the following "
"code within the Text/HTML editor:"
msgstr ""

#: library/about.php:184
msgid ""
"&lt;blockquote class=\"pullquote\"&gt;This is a pull quote that will appear "
"within your text.&lt;/blockquote&gt;"
msgstr ""

#: library/about.php:185
msgid "You can also align it to the right with this code:"
msgstr ""

#: library/about.php:187
msgid ""
"&lt;blockquote class=\"pullquote alignright\"&gt;This is a pull quote that "
"will appear within your text.&lt;/blockquote&gt;"
msgstr ""

#: library/about.php:191
#, php-format
msgid ""
"For more information, check out the %sDocumentation &amp; FAQs%s section on "
"my site."
msgstr ""

#: library/customizer.php:54
msgid "Display Tagline"
msgstr ""

#: library/customizer.php:60
msgid "Layout"
msgstr ""

#: library/customizer.php:70
msgid "Site Width"
msgstr ""

#: library/customizer.php:75
msgid "960px"
msgstr ""

#: library/customizer.php:76
msgid "640px"
msgstr ""

#: library/customizer.php:77
msgid "Full Width"
msgstr ""

#: library/customizer.php:87
msgid "Site Layout"
msgstr ""

#: library/customizer.php:91
msgid "Left Sidebar"
msgstr ""

#: library/customizer.php:92
msgid "Right Sidebar"
msgstr ""

#: library/customizer.php:93
msgid "No Sidebar"
msgstr ""

#: library/customizer.php:103
msgid "Main Content"
msgstr ""

#: library/customizer.php:107
msgid "1 Column"
msgstr ""

#: library/customizer.php:108
msgid "2 Columns"
msgstr ""

#: library/customizer.php:109
msgid "3 Columns"
msgstr ""

#: library/customizer.php:110
msgid "4 Columns"
msgstr ""

#: library/customizer.php:111
msgid "5 Columns"
msgstr ""

#: library/customizer.php:112
msgid "6 Columns"
msgstr ""

#: library/customizer.php:113
msgid "7 Columns"
msgstr ""

#: library/customizer.php:114
msgid "8 Columns"
msgstr ""

#: library/customizer.php:115
msgid "9 Columns"
msgstr ""

#: library/customizer.php:116
msgid "10 Columns"
msgstr ""

#: library/customizer.php:117
msgid "11 Columns"
msgstr ""

#: library/customizer.php:118
msgid "12 Columns"
msgstr ""

#: library/customizer.php:132
msgid "Teaser Excerpt"
msgstr ""

#: library/customizer.php:133
msgid "Full Content"
msgstr ""

#: library/customizer.php:139
msgid "Posts"
msgstr ""

#: library/customizer.php:149
msgid "Display Categories"
msgstr ""

#: library/customizer.php:160
msgid "Display Author"
msgstr ""

#: library/customizer.php:171
msgid "Display Date"
msgstr ""

#: library/customizer.php:182
msgid "Display Comment Count"
msgstr ""

#: library/customizer.php:194
msgid "Link Color"
msgstr ""

#: library/preview-pro.php:19
#, php-format
msgid "Upgrade to %s Pro"
msgstr ""

#: library/preview-pro.php:19
msgid "Upgrade to Pro"
msgstr ""

#: library/preview-pro.php:85
#, php-format
msgid ""
"Take your site to the next level with the full version of %s. Check out some "
"of the advanced features that&rsquo;ll give you more control over your "
"site&rsquo;s layout and design."
msgstr ""

#: library/preview-pro.php:97
msgid "Advanced Color Picker"
msgstr ""

#: library/preview-pro.php:102
#, php-format
msgid ""
"Sometimes the default colors just aren&rsquo;t working for you. In %s you "
"can use the advanced color picker to make sure you get the exact colors you "
"want."
msgstr ""

#: library/preview-pro.php:103
msgid ""
"Easily select one of the eight preset colors or dive even deeper into your "
"customization by using a more specific hex code."
msgstr ""

#: library/preview-pro.php:108
msgid "Google Fonts"
msgstr ""

#: library/preview-pro.php:113
msgid ""
"Web-safe fonts are a thing of the past, so why not try to spice things up a "
"bit?"
msgstr ""

#: library/preview-pro.php:114
msgid ""
"Choose from some of Google Fonts&rsquo; most popular fonts to improve your "
"site&rsquo;s typeface readability and make things look even more amazing."
msgstr ""

#: library/preview-pro.php:119
msgid "Extended Widgetized Footer"
msgstr ""

#: library/preview-pro.php:124
msgid ""
"If you need to include more widgets on your site, take advantage of the "
"Extended Footer."
msgstr ""

#: library/preview-pro.php:125
msgid ""
"Use the Theme Options customizer to set the number of columns you want to "
"appear. You can also customize your site&rsquo;s copyright notice."
msgstr ""

#: library/preview-pro.php:130
msgid "Custom CSS Editor"
msgstr ""

#: library/preview-pro.php:135
msgid ""
"Sometimes the Theme Options don't let you control everything you want. "
"That's where the Custom CSS Editor comes into play."
msgstr ""

#: library/preview-pro.php:136
msgid ""
"Use CSS to style any element without having to worry about losing your "
"changes when you update. All your Custom CSS is safely stored in the "
"database."
msgstr ""

#: library/preview-pro.php:141
msgid "Even More Theme Options"
msgstr ""

#: library/preview-pro.php:144
msgid "Full Width Posts/Pages"
msgstr ""

#: library/preview-pro.php:145
msgid ""
"Each page/post has an option to remove both sidebars so you can use the full "
"width of your site to display whatever you want."
msgstr ""

#: library/preview-pro.php:148
msgid "Multiple Sidebar Layouts"
msgstr ""

#: library/preview-pro.php:149
msgid ""
"Sometimes one sidebar just isn&rsquo;t enough, so add a second one and place "
"it where you want."
msgstr ""

#: library/preview-pro.php:155
msgid "Bootstrap Shortcodes"
msgstr ""

#: library/preview-pro.php:156
#, php-format
msgid ""
"Shortcodes are awesome and easy to use. That&rsquo;s why %s comes with a "
"bunch, like a slideshow carousel, alert boxes and more."
msgstr ""

#: library/preview-pro.php:159
msgid "Import/Export Tool"
msgstr ""

#: library/preview-pro.php:160
msgid ""
"Once you&rsquo;ve set up your site exactly how you want, you can easily "
"export the Theme Options and Custom CSS for safe keeping."
msgstr ""

#: library/preview-pro.php:166
#, php-format
msgid "Buy %s Now &rarr;"
msgstr ""

#: search.php:18
msgid " search results for"
msgstr ""

#: sidebar.php:13
#, php-format
msgid "Add your own widgets by going to the %sWidgets admin page%s."
msgstr ""

#: sidebar.php:17
msgid "Meta"
msgstr ""

#: sidebar.php:26
msgid "Archives"
msgstr ""

#: single.php:10
msgid "Post navigation"
msgstr ""

#: single.php:12
msgid "&larr; Previous Image"
msgstr ""

#: single.php:13
msgid "Next Image &rarr;"
msgstr ""

#: single.php:15
msgid "&larr; %title"
msgstr ""

#: single.php:16
msgid "%title &rarr;"
msgstr ""

#. Theme Name of the plugin/theme
msgid "Gridiculous"
msgstr ""

#. Theme URI of the plugin/theme
msgid "https://themes.bavotasan.com/themes/gridiculous-pro-wordpress-theme/"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Gridiculous is a lightweight HTML5 responsive theme based on the grid layout "
"boilerplate of the same name. Use the new Theme Options customizer to add "
"your own header image, custom background, page layout, site width, link "
"color and more. Distinguish each post with one of the eight supported post "
"formats: Video, Image, Aside, Status, Audio, Quote, Link and Gallery. "
"Install JetPack to display each of your galleries through a tiled view and "
"jQuery carousel. Uses Normalize.css for cross browser compatibility and two "
"Google Fonts for improved typeface readability. Compatible with bbPress & "
"BuddyPress. Works perfectly in desktop browsers, tablets and handheld "
"devices. For a live demo go to http://demos.bavotasan.com/gridiculous/."
msgstr ""

#. Author of the plugin/theme
msgid "c.bavota"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://bavotasan.com"
msgstr ""

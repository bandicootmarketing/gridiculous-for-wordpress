<?php
/**
 * Set up the default theme options
 *
 * @param	string $name  The option name
 *
 * @since Gridiculous 1.0.0
 */
function bavotasan_default_theme_options() {
	//delete_option( 'theme_mods_gridiculous' );
	return array(
		'width' => 'w960',
		'layout' => '2',
		'primary' => 'c8',
		'tagline' => 'on',
		'display_author' => 'on',
		'display_date' => 'on',
		'display_comment_count' => 'on',
		'display_categories' => 'on',
		'link_color' => '#333333',
		'excerpt_content' => 'content',
	);
}

function bavotasan_theme_options() {
	$bavotasan_default_theme_options = bavotasan_default_theme_options();

	$return = array();
	foreach( $bavotasan_default_theme_options as $option => $value ) {
		$return[$option] = get_theme_mod( $option, $value );
	}

	return $return;
}

class Bavotasan_Customizer {
	public function __construct() {
		add_action( 'customize_register', array( $this, 'customize_register' ) );

		$mods = get_option( 'theme_mods_gridiculous' );
		if ( empty( $mods ) ) {
			add_option( 'theme_mods_gridiculous', get_option( 'gridiculous_theme_options' ) );
		}
	}

	public function customize_register( $wp_customize ) {
		$bavotasan_default_theme_options = bavotasan_default_theme_options();
		$wp_customize->add_setting( 'tagline', array(
			'default' => $bavotasan_default_theme_options['tagline'],
            'sanitize_callback' => array( $this, 'sanitize_checkbox' ),
		) );

		$wp_customize->add_control( 'tagline', array(
			'label'      => __( 'Display Tagline', 'gridiculous' ),
			'section'    => 'title_tagline',
			'type' => 'checkbox',
		) );

		$wp_customize->add_section( 'bavotasan_layout', array(
			'title' => __( 'Layout', 'gridiculous' ),
			'priority' => 35,
		) );

		$wp_customize->add_setting( 'width', array(
			'default' => $bavotasan_default_theme_options['width'],
			'sanitize_callback' => 'esc_attr',
		) );

		$wp_customize->add_control( 'width', array(
			'label'      => __( 'Site Width', 'gridiculous' ),
			'section'    => 'bavotasan_layout',
			'type' => 'select',
			'choices' => array(
				'' => '1200px',
				'w960' => __( '960px', 'gridiculous' ),
				'w640' => __( '640px', 'gridiculous' ),
				'wfull' => __( 'Full Width', 'gridiculous' ),
			),
		) );

		$wp_customize->add_setting( 'layout', array(
			'default' => $bavotasan_default_theme_options['layout'],
			'sanitize_callback' => 'esc_attr',
		) );

		$wp_customize->add_control( 'layout', array(
			'label'      => __( 'Site Layout', 'gridiculous' ),
			'section'    => 'bavotasan_layout',
			'type' => 'radio',
			'choices' => array(
				'1' => __( 'Left Sidebar', 'gridiculous' ),
				'2' => __( 'Right Sidebar', 'gridiculous' ),
				'3' => __( 'No Sidebar', 'gridiculous' ),
			),
		) );

		$wp_customize->add_setting( 'primary', array(
			'default' => $bavotasan_default_theme_options['primary'],
			'sanitize_callback' => 'esc_attr',
		) );

		$wp_customize->add_control( 'primary', array(
			'label'      => __( 'Main Content', 'gridiculous' ),
			'section'    => 'bavotasan_layout',
			'type' => 'select',
			'choices' => array(
				'c1' => __( '1 Column', 'gridiculous' ),
				'c2' => __( '2 Columns', 'gridiculous' ),
				'c3' => __( '3 Columns', 'gridiculous' ),
				'c4' => __( '4 Columns', 'gridiculous' ),
				'c5' => __( '5 Columns', 'gridiculous' ),
				'c6' => __( '6 Columns', 'gridiculous' ),
				'c7' => __( '7 Columns', 'gridiculous' ),
				'c8' => __( '8 Columns', 'gridiculous' ),
				'c9' => __( '9 Columns', 'gridiculous' ),
				'c10' => __( '10 Columns', 'gridiculous' ),
				'c11' => __( '11 Columns', 'gridiculous' ),
				'c12' => __( '12 Columns', 'gridiculous' ),
			),
		) );

		$wp_customize->add_setting( 'excerpt_content', array(
			'default' => $bavotasan_default_theme_options['excerpt_content'],
			'sanitize_callback' => 'esc_attr',
		) );

		$wp_customize->add_control( 'excerpt_content', array(
			'label'      => __( 'Post Content Display', 'gridiculous' ),
			'section'    => 'bavotasan_layout',
			'type' => 'radio',
			'choices' => array(
				'excerpt' => __( 'Teaser Excerpt', 'gridiculous' ),
				'content' => __( 'Full Content', 'gridiculous' ),
			),
		) );

		// Posts panel
		$wp_customize->add_section( 'gridiculous_posts', array(
			'title' => __( 'Posts', 'gridiculous' ),
			'priority' => 45,
		) );

		$wp_customize->add_setting( 'display_categories', array(
			'default' => $bavotasan_default_theme_options['display_categories'],
            'sanitize_callback' => array( $this, 'sanitize_checkbox' ),
		) );

		$wp_customize->add_control( 'display_categories', array(
			'label' => __( 'Display Categories', 'gridiculous' ),
			'section' => 'gridiculous_posts',
			'type' => 'checkbox',
		) );

		$wp_customize->add_setting( 'display_author', array(
			'default' => $bavotasan_default_theme_options['display_author'],
            'sanitize_callback' => array( $this, 'sanitize_checkbox' ),
		) );

		$wp_customize->add_control( 'display_author', array(
			'label' => __( 'Display Author', 'gridiculous' ),
			'section' => 'gridiculous_posts',
			'type' => 'checkbox',
		) );

		$wp_customize->add_setting( 'display_date', array(
			'default' => $bavotasan_default_theme_options['display_date'],
            'sanitize_callback' => array( $this, 'sanitize_checkbox' ),
		) );

		$wp_customize->add_control( 'display_date', array(
			'label' => __( 'Display Date', 'gridiculous' ),
			'section' => 'gridiculous_posts',
			'type' => 'checkbox',
		) );

		$wp_customize->add_setting( 'display_comment_count', array(
			'default' => $bavotasan_default_theme_options['display_comment_count'],
            'sanitize_callback' => array( $this, 'sanitize_checkbox' ),
		) );

		$wp_customize->add_control( 'display_comment_count', array(
			'label' => __( 'Display Comment Count', 'gridiculous' ),
			'section' => 'gridiculous_posts',
			'type' => 'checkbox',
		) );

		// Color options
		$wp_customize->add_setting( 'link_color', array(
			'default' => $bavotasan_default_theme_options['link_color'],
			'sanitize_callback' => 'sanitize_hex_color',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'link_color', array(
			'label' => __( 'Link Color', 'gridiculous' ),
			'section' => 'colors',
		) ) );
	}

	/**
	 * Sanitize checkbox options
	 *
	 * @since 1.0.2
	 */
    public function sanitize_checkbox( $value ) {
        if ( 'on' != $value )
            $value = false;

        return $value;
    }
}
$bavotasan_customizer = new Bavotasan_Customizer;
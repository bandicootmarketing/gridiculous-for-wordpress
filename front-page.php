<?php get_header(); ?>

	<?php
	global $paged;
	if ( 2 > $paged ) {
		// Display home page top widgetized area
		if ( is_active_sidebar( 'home-page-top-area' ) ) {
			?>
			<div id="home-page-widgets" class="c12 clearfix">
				<div class="row">
					<?php dynamic_sidebar( 'home-page-top-area' ); ?>
				</div>
			</div>
			<?php
		}

		$sticky = get_option( 'sticky_posts' );
		$featured = new WP_Query( array(
			'posts_per_page' => 1,
			'post__in'  => $sticky,
			'ignore_sticky_posts' => 1
		) );
		if ( ! empty( $sticky[0] ) ) {
			?>

			<div id="featured">
				<div class="c12">
					<?php
					while ( $featured->have_posts() ) : $featured->the_post();
						get_template_part( 'content', get_post_format() );
					endwhile;

					wp_reset_postdata();
					?>
				</div>
			</div>

			<?php
		}
	}
	?>

	<div id="primary" <?php bavotasan_primary_attr(); ?>>

		<?php
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				get_template_part( 'content', get_post_format() );
			endwhile;

			bavotasan_pagination();
		endif;
		?>

	</div><!-- #primary -->

<?php get_footer(); ?>